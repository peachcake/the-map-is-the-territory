#lang pollen

◊define-meta[title]{Introduction}

Beneath the visual surface of your computer is an old and powerful magic, a silent but quick stream of energy that the computer draws from for its power.  It is hidden but always present,  like the sacred well held in the base of a cathedral.

This place has many names: Shell, Terminal, Bash, Zsh, The Command Line. All of these names are correct but incomplete, accurate to a part but unable to describe the whole.  Like all magical things, there are aspects of the command line that will forever be beyond our articulation.

Too, Like too many magical things, the secular world continually tries to defang and deform it.  Modern tech culture will describe the command line as an obscure productivity tool, something you learn only to impress other tech folk(a meaningless activity) or to become a power user (a meaningless phrase).  Conventional tech wisdom will tell you the command line is an intimidating, obscure, imposing place--something that is impossible to learn and dangerous to use.  This is only an attempt to dull its glow, to hide its true nature, that the command line is a world made entirely from the first occult technology: the word.

The command line is pure language, and to exist in it is to exist as language, to practice all the reality-shifting and world manifesting power of metaphor and dialogue.  This is a place of empowerment, tangible creativity, and mystic bewilderment and while it can be dangerous, it's also exceedingly helfpul if you know how to listen, and how to ask questions.  This place is waiting for you, but will not make itself known until you ask for it.  You must make the first step.

I love the terminal, the shell, bash, the command line.  I discovered it during a time of extreme digital burnout and this discovery was honestly one of hte most important parts of my life.  It's only been with time, and increased familiarity with all the nuances of this place, that made me realize that none of the above is hyperbolic.  Because you are cool, and likely my friend (or future friend!), I want to share the command line with you.

This zine is an introduction to interacting with your computer through the terminal, and a hint towards all the beautiful radical power possible in it.  In it, we will learn some basics of the command line through the act of spellwork, and by the end you will have literally gathered fragments of a spell from the ether and cast it from a modest, digital altar.  At the same time, this zine will teach the same terms and abilities you'd find in a technical and you can use the skills gained in this to do practical, business-like things.  That does not detract from the magic inherent in the commands; the tarot can be great for card games, and many magical herbs taste great in sauces and work incredibly as soaps.

Hopefully I'm able to share a bit of the wonder and delight of the terminal with you and that it's meaningful for you.  Let me know if so, and keep it to yrself if not!
